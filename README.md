# Stripe Masterclass Server Repo

This repo contains the Stripe Masterclass server project files.

## Important

Create file .env and add the next variables enviroment:

SECRET_KEY= 

WEB_APP_URL=   
                                                                           
WEB_HOOK_SECRET=
                         
GOOGLE_APPLICATION_CREDENTIALS=

## Run


´´´
npm install
´´´

´´´
npm start
´´´

If you want run webhook unzip file stripe and run:


´´´
./stripe listen --forward-to http://localhost:8080/webhook
´´´


https://www.udemy.com/course/stripe-masterclass-with-react-node/
 
